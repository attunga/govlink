package utils

import (
	"bytes"
	"log"
	"net"
	"os/exec"
	"runtime"
	"strings"
)

func GetUniqueIDStringKey() string {

	outboundIP := getOutboundIP()
	//fmt.Println("Outbound IP:", outboundIP)

	outBoundMAC := getMacAddr()
	//fmt.Println("Outbound MAC:", outBoundMAC)

	machineID := getMachineID()
	//fmt.Println("Machine ID:", machineID)

	// Get a Calculated ID for this machine - this will become the aes key
	s := []string{string(machineID), string(outBoundMAC), outboundIP.String()}
	calculatedID := strings.Join(s, "-")

	//fmt.Println("Calculated ID: ", calculatedID)

	// Key variable is global
	paddedKey := Make32bits(calculatedID)

	return paddedKey
}

// Try to get the machine ID .. may not work on Windows,  not a big deal,  it can just use IP and MAC.
func getMachineID() string {

	if runtime.GOOS == "linux" {

		out, _ := exec.Command("cat", "/var/lib/dbus/machine-id").Output()
		out2, _ := exec.Command("cat", "/etc/machine-id").Output()

		output := string(out[:])
		output2 := string(out2[:])

		if output != "" {
			return strings.Trim(output, "\n")
		} else if output2 != "" {
			return strings.Trim(output2, "\n")
		} else {
			return "Machine_ID_Not_Found"
		}

	} else {
		return runtime.GOOS
	}

}

// Get preferred outbound ip of this machine
func getOutboundIP() net.IP {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}

// getMacAddr gets the MAC hardware
// address of the host machine
func getMacAddr() (addr string) {
	interfaces, err := net.Interfaces()
	if err == nil {
		for _, i := range interfaces {
			if i.Flags&net.FlagUp != 0 && bytes.Compare(i.HardwareAddr, nil) != 0 {
				// Don't use random as we have a real address
				addr = i.HardwareAddr.String()
				break
			}
		}
	}
	return
}
