package utils

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func GetParameter(cmd *cobra.Command, parameter string) string {

	parameterFoundValue := ""

	// If the flag is passsed for the collector then we value that over the default from other locations.
	if cmd.Flag(parameter).Changed {
		parameterFoundValue = cmd.Flag(parameter).Value.String()
	}

	// If not passed then we try to display the value from a config file
	if parameterFoundValue == "" {
		//fmt.Println("Trying to display Viper value from ", viper.ConfigFileUsed())
		parameterFoundValue = viper.GetString(parameter)
	}

	// If there is no parameter passed and there is nothing in the Config File then we go for Command Defaults.
	if parameterFoundValue == "" {
		parameterFoundValue = cmd.Flag(parameter).Value.String()
	}
	//fmt.Println("Collector Type:", collectorType)

	return parameterFoundValue
}

func CheckErr(err error) {
	if err != nil {
		fmt.Println("Error Message: ", err.Error())
		panic(err)
	}
}
