// Copyright © 2019 Lindsay Steele <lgsteele@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"govlink/collector"
	"govlink/utils"
	"os"
)

// hashCmd represents the hash command
var hashCmd = &cobra.Command{
	Use:   "hash",
	Short: "Generate a integer hash of domains and levels found.  Mostly used for monitoring purposes",
	Long: `This command generates an integer hash combined domains and security levels. 
Configuration is limited as it is expected that this command is run in an automated manner
for monitoring when two data sources may be out of sync.

Examples:  govlink hash`,
	Run: func(cmd *cobra.Command, args []string) {

		var domSecRecords collector.DomainSecurityRecordsList
		collectorType := utils.GetParameter(cmd, "collector")

		serverAddress := viper.GetString("serverAddress")

		// Future Case Statement here to cover where they are coming from

		// Get Data from Required Source.   Most settings should come from a config file
		// as the hash function should be automated.

		switch collectorType {
		case "http":
			domSecRecords = collector.GetViaHTTP(serverAddress)
			fmt.Println(domSecRecords.GetHash())
		case "db":
			dbName := viper.GetString("dbName")
			dbUsername := viper.GetString("dbUsername")
			dbPassword := ""
			if viper.GetBool("dbPasswordIsEncrypted") {
				key := []byte(utils.GetUniqueIDStringKey())
				dbPassword = utils.Decrypt(key, viper.GetString("dbEncryptedPassword"))
			} else {
				dbPassword = viper.GetString("dbPassword")
			}
			domSecRecords = collector.GetViaDB(serverAddress, dbName, dbUsername, dbPassword)
			domSecRecords.LoadCoreDomains()
			fmt.Println(domSecRecords.GetHashDB())
		case "https":
			//domSecRecords := collector.GetViaHTTP()
		default:
			fmt.Println("Unknown Collector Type - valid options are http or db")
			os.Exit(1)
		}

		// Get thje hash

	},
}

func init() {
	rootCmd.AddCommand(hashCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// hashCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// hashCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
