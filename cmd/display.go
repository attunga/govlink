// Copyright © 2019 Lindsay Steele <lgsteele@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"govlink/display"
	"govlink/utils"

	"github.com/spf13/cobra"
)

// displayCmd represents the display command
var displayCmd = &cobra.Command{
	Use:   "display",
	Short: "Displays records to screen in a number of formats",
	Long: `Options are 
  --format csv, full or core
  -- order 
csv:  Give a comma seperated format, used for basic display and scripted comparison
full: shows a full listing of available information
core:  Shows the results of the domains removed after the exceptionDomains are loaded.   This is useful when trying 
to understand what an issue might be with regard to hash values.

Examples: 

Show listing in csv format
govlink display  

Show full listing sorted by organisation 
govlink display -f=full --order=seclevel

Show Core Domains when connected to DB
govlink display --format=core

`,
	Run: func(cmd *cobra.Command, args []string) {
		//fmt.Println("display called")

		collectorType := utils.GetParameter(cmd, "collector")

		serverAddress := utils.GetParameter(cmd, "serverAddress")

		displayFormat := utils.GetParameter(cmd, "format")

		displayOrder := utils.GetParameter(cmd, "sortorder")

		if rootCmd.Flag("debug").Changed {
			fmt.Println(collectorType, serverAddress, displayFormat)
		}

		// We should look at the display format here .. and work out what parameters are needed .. potentially just

		display.DisplayRecordsToScreen(collectorType, serverAddress, displayFormat, displayOrder, cmd)
	},
}

func init() {
	rootCmd.AddCommand(displayCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// displayCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// displayCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	displayCmd.PersistentFlags().StringP("format", "f", "csv", "Output Display Format - Currently csv, full or core for DB ")
	displayCmd.PersistentFlags().StringP("sortorder", "o", "domain", "Sort Order - domain, org, seclevel or sec-org")
}
