// Copyright © 2019 Lindsay Steele <lgsteele@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"govlink/utils"
)

// encryptCmd represents the encrypt command
var encryptCmd = &cobra.Command{
	Use:   "encrypt",
	Short: "Encrypt the password used for database connection. ",
	Long: `This reads from unencrypted db password in config file and prints an encrypted password to screen.
Be aware that passwords are generated for only the computer that the passwords are read from.

To use this function set a password for the db in the config file such as:

dbPassword: "password"

This password will then be displayed to screen.  You should then copy this password to the config item:

dbEncryptedPassword: "_lxh-F9u298odayktZPPCPQFD6GV"

At this stage you can then remove the dbPassword setting.

Then change the following setting to True to allow the program to read the Encrypted Password

dbPasswordIsEncrypted: True
`,
	Run: func(cmd *cobra.Command, args []string) {
		// extract the hardware information base on the interface name
		// capture above
		key := utils.GetUniqueIDStringKey()
		fmt.Println("key: ", key, len(key))
		encryptedDBPassword := utils.Encrypt([]byte(key), viper.GetString("dbPassword"))
		fmt.Println("Encrypted Password: ", encryptedDBPassword)
	},
}

func init() {
	rootCmd.AddCommand(encryptCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// encryptCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// encryptCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}
