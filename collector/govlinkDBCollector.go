package collector

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"govlink/utils"
	"strconv"
)

// Gets a list of domains from a Database Source

type govlinkDBCollector struct {
	// constructor variable that can be set on creation from settings
	DBAddress  string
	DBName     string
	DBUsername string
	DBPassword string
}

func (d govlinkDBCollector) getDomainSecurityList() DomainSecurityRecordsList {

	// Go off to the other functions here to display the data and return either the data or an error

	// Get the list of values we can from the HTTP Scrape

	connectionString := d.DBUsername + ":" + d.DBPassword + "@" + d.DBAddress + "/" + d.DBName

	//fmt.Println(connectionString)

	domSecRecords := getRecordsFromMySQLDatabase(connectionString)

	return domSecRecords
}

func getRecordsFromMySQLDatabase(connectionString string) DomainSecurityRecordsList {

	dsr := DomainSecurityRecordsList{}

	var govLinkOrg = dommainSecurityRecord{}
	db, err := sql.Open("mysql", connectionString)
	utils.CheckErr(err)

	//fmt.Println(db)

	defer db.Close()

	// Execute the query
	//results, err := db.Query("SELECT id, name FROM tags")
	results, err := db.Query("select domain_name, threshold from domain_threshold where threshold > 0")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	stringToInt := 99

	for results.Next() {
		var domain_name, domain_threshold string
		// for each row, scan the result into our tag composite object
		err = results.Scan(&domain_name, &domain_threshold)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		// and then print out the tag's Name attribute
		//fmt.Println("Email From:",email_from,"Email To:",email_to)
		stringToInt, _ = strconv.Atoi(domain_threshold)
		govLinkOrg.securityLevelInt = stringToInt
		govLinkOrg.domainSuffix = domain_name
		//govLinkOrg.securityLevelString = functionToConvert .. from Firstwave List
		dsr.AddDomainSecurtiyRecord(govLinkOrg)
	}

	return dsr
}
