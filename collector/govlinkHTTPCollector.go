package collector

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"strings"
)

//  Gets a list of domains from a HTTP Source
// Conforms to govlinkCollector interface
type govlinkHTTPCollector struct {
	// constructor variable that can be set on creation from settings
	HTTPLocation string
}

func (c govlinkHTTPCollector) getDomainSecurityList() DomainSecurityRecordsList {

	// Go off to the other functions here to display the data and return either the data or an error

	// Get the list of values we can from the HTTP Scrape
	domSecRecords := scrapeGovLinkOrganisations(c.HTTPLocation)

	return domSecRecords
}

func scrapeGovLinkOrganisations(serverAddress string) DomainSecurityRecordsList {

	// Needs fixing to display rid the the deprecation
	doc, err := goquery.NewDocument(serverAddress)
	if err != nil {
		log.Fatal(err)
		// Should Exit here ...
	}

	dsr := DomainSecurityRecordsList{}

	//c := Circle{x: 0, y: 0, r: 5}

	// Creates the initial goLink Variable initialised to default values of error
	govLinkOrg := govLinkOrgDefaults()
	var headings, row []string
	var rows [][]string
	cellNumber := 0 // Just Counts the Cell number in the Row

	// Find each table .. we hope there is only ever one table on the page
	doc.Find("table").Each(func(index int, tablehtml *goquery.Selection) {
		// Find the Row in the Tables
		tablehtml.Find("tr").Each(func(indextr int, rowhtml *goquery.Selection) {
			// Looks for a Table header which we don't care too much about
			rowhtml.Find("th").Each(func(indexth int, tableheading *goquery.Selection) {
				headings = append(headings, tableheading.Text())
			})
			// Look for the table data in the Row
			rowhtml.Find("td").Each(func(indexth int, tablecell *goquery.Selection) {
				cellNumber++
				//fmt.Println("RowBit:", cellNumber,tablecell.Text())
				row = append(row, tablecell.Text())
				// Fill Current Struct
				switch cellNumber {
				case 1:
					govLinkOrg.organisation = strings.Trim(tablecell.Text(), " ")
				case 2:
					govLinkOrg.securityLevelString = strings.Trim(tablecell.Text(), " ")
					govLinkOrg.securityLevelInt = getSecurityLevelNumber(tablecell.Text())
				case 3:
					govLinkOrg.service = strings.Trim(tablecell.Text(), " ")
				case 4:
					govLinkOrg.domainSuffix = getDomainSuffix(tablecell.Text())
				default:
					fmt.Println("Error Detected - Unknown Table Cell")
				}

			})
			// End of the Row
			// Just check that this is not an error row - most likely
			if govLinkOrg.domainSuffix != "error" {
				dsr.AddDomainSecurtiyRecord(govLinkOrg)
			}
			govLinkOrg = govLinkOrgDefaults() // Sets back to Default Values
			rows = append(rows, row)
			row = nil
			cellNumber = 0
			//fmt.Println("Start New Row")
		})
	})
	//fmt.Println("####### headings = ", len(headings), headings)
	//fmt.Println("####### rows = ", len(rows), rows)

	// Get rid of the Faulty First Records

	// Sort by Domain Name
	dsr.SortByDomain()

	return dsr
}

// Sets struct to default to detect errors .. gota be a neater way to do this.
func govLinkOrgDefaults() dommainSecurityRecord {

	var govLinkOrg = dommainSecurityRecord{}

	govLinkOrg.domainSuffix = "error"
	govLinkOrg.organisation = "error"
	govLinkOrg.service = "error"
	govLinkOrg.securityLevelString = "error"

	return govLinkOrg
}

// Just display the domain out of the string - could use regex but this is simple
func getDomainSuffix(text string) string {

	components := strings.Split(text, "@")
	domain := strings.Trim(components[1], " ")
	//fmt.Println("Suffix", domain)
	return domain
}

func getSecurityLevelNumber(text string) int {
	returnValue := 100 // The classification return value - 100 is for debugging
	securityLevel := strings.ToLower(text)

	switch {
	case strings.HasPrefix(securityLevel, "unofficial"):
		returnValue = 0
	case strings.HasPrefix(securityLevel, "unclassified"):
		returnValue = 1
	case strings.HasPrefix(securityLevel, "protected"):
		returnValue = 3
	default:
		returnValue = 99 // error has occured
	}

	return returnValue
}
