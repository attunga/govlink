package collector

func ContainsDomSec(currentList []dommainSecurityRecord, newDomain string) bool {
	for i, _ := range currentList {
		if currentList[i].domainSuffix == newDomain {
			//fmt.Println("Found the Same: ", currentList[i].domainSuffix, " and ", newDomain)
			return true
		}
	}
	return false

}
