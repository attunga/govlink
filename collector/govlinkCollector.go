package collector

import (
	"fmt"
	"github.com/spf13/viper"
	"govlink/utils"
	"hash/crc32"
	"hash/crc64"
	"os"
	"sort"
	"strconv"
	"text/tabwriter"
	"time"
	"strings"
)

// Interface for a govlinkCollector
type govlinkCollector interface {
	// Get Data that goes off and gets the data and returns a slice of records
	getDomainSecurityList() DomainSecurityRecordsList
}

// Generic Function that uses the interface above to display a list of domain security records from a range of
// collection areas
func GetDomainSecurityRecords(glc govlinkCollector) DomainSecurityRecordsList {
	return glc.getDomainSecurityList()
}

// Holds a Single Records of a domain security .. May need to expand this for Database
type dommainSecurityRecord struct {
	organisation        string
	securityLevelString string
	securityLevelInt    int
	service             string
	domainSuffix        string
}

// Holds the Collection of Domain Security Records together with a bit of metadata about the record
type DomainSecurityRecordsList struct {
	id                        int
	checkDate                 time.Time
	domainSecurityRecords     []dommainSecurityRecord
	domainSecurityRecordsCore []dommainSecurityRecord // A list of domains with Exceptions Removed used for Hashing
	exceptionDomains          []string                // A list of Exception Domains - Removed before hashing or printing
	exceptionsLoaded          bool                    // whether we have loaded the exceptions or not
}

func (domSecRecords *DomainSecurityRecordsList) NumberOfDomainSecurtiyRecords() int {
	return len(domSecRecords.domainSecurityRecords)
}

// Adds a record,  returns true if it was sucessful
func (domSecRecords *DomainSecurityRecordsList) AddDomainSecurtiyRecord(domrec dommainSecurityRecord) {
	// Only add if the record is not currently there
	if !ContainsDomSec(domSecRecords.domainSecurityRecords, domrec.domainSuffix) {
		domSecRecords.domainSecurityRecords = append(domSecRecords.domainSecurityRecords, domrec)
	}
}

// Adds a record,  returns true if it was sucessful
func (domSecRecords *DomainSecurityRecordsList) AddDomainSecurtiyRecordCore(domrec dommainSecurityRecord) {
	// Only add if the record is not currently there
	if !ContainsDomSec(domSecRecords.domainSecurityRecordsCore, domrec.domainSuffix) {
		domSecRecords.domainSecurityRecordsCore = append(domSecRecords.domainSecurityRecordsCore, domrec)
	}
}

func (domSecRecords *DomainSecurityRecordsList) RemoveDuplicates() {

	// Here we Remove the Duplicates that we have received.
}

func (domSecRecords *DomainSecurityRecordsList) SortByDomain() {
	// Sort by domain name - with a sort by lower case to overcome to consistencies in the way that a Govlink domain is displayed with lower case.
	sort.SliceStable(domSecRecords.domainSecurityRecords, func(i, j int) bool {
		return strings.toLower(domSecRecords.domainSecurityRecords[i].domainSuffix) < strings.toLower(domSecRecords.domainSecurityRecords[j].domainSuffix)
	})
}

func (domSecRecords *DomainSecurityRecordsList) SortByOrg() {
	// Sort by organisation name
	sort.SliceStable(domSecRecords.domainSecurityRecords, func(i, j int) bool {
		return domSecRecords.domainSecurityRecords[i].organisation < domSecRecords.domainSecurityRecords[j].organisation
	})
}

func (domSecRecords *DomainSecurityRecordsList) SortBySecuritylevel() {
	// Sort by organisation name
	sort.SliceStable(domSecRecords.domainSecurityRecords, func(i, j int) bool {
		return domSecRecords.domainSecurityRecords[i].securityLevelInt < domSecRecords.domainSecurityRecords[j].securityLevelInt
	})
}

func (domSecRecords *DomainSecurityRecordsList) PrintToScreenCSV() {
	for _, domSecRecord := range domSecRecords.domainSecurityRecords {
		fmt.Print(domSecRecord.domainSuffix, ",", strconv.Itoa(domSecRecord.securityLevelInt), "\n")
	}
}

func (domSecRecords *DomainSecurityRecordsList) PrintToScreenCSVCore() {
	domSecRecords.LoadCoreDomains()
	for _, domSecRecord := range domSecRecords.domainSecurityRecordsCore {
		fmt.Print(domSecRecord.domainSuffix, ",", strconv.Itoa(domSecRecord.securityLevelInt), "\n")
	}
}

func (domSecRecords *DomainSecurityRecordsList) PrintToScreenFull() {

	w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.Debug)
	fmt.Fprintln(w, "Domain \t Security Level \t Sec Level Int \t Organisation Name")
	for _, domSecRecord := range domSecRecords.domainSecurityRecords {

		fmt.Fprintln(w, domSecRecord.domainSuffix, "\t", domSecRecord.securityLevelString, "\t", strconv.Itoa(domSecRecord.securityLevelInt), "\t", domSecRecord.organisation)
	}
	w.Flush()
}

// Exceptions Domains are used at the DB side to clean the list for Monitoring
func (domSecRecords *DomainSecurityRecordsList) LoadExceptions() {

	// Check if exceptions are loaded
	if domSecRecords.exceptionsLoaded {
		return
	}

	// Try to Load the List of Exceptions
	exceptionDomains := viper.GetStringSlice("exceptionDomains")

	if len(exceptionDomains) > 0 {
		domSecRecords.exceptionDomains = exceptionDomains
	}

	domSecRecords.exceptionsLoaded = true
}

func (domSecRecords *DomainSecurityRecordsList) GetExceptions() {

	// Load the List of Exceptions First .. check whether it is empty or not.

}

// These are domains minus exceptions.  Used for Hashing most of the time
func (domSecRecords *DomainSecurityRecordsList) LoadCoreDomains() {

	// Try to load exceptions ... just in case they are not loaded
	domSecRecords.LoadExceptions()

	// if there are no exceptions then just return
	if len(domSecRecords.exceptionDomains) < 1 {
		return
	}

	// loop through the currently found domains ....

	for _, domain := range domSecRecords.domainSecurityRecords {
		//fmt.Println("Processing:", domain.domainSuffix)
		if !utils.Contains(domSecRecords.exceptionDomains, domain.domainSuffix) {
			domSecRecords.AddDomainSecurtiyRecordCore(domain)
			//fmt.Println("Adding Domain:", domain.domainSuffix)
		}
	}

	// if the domain exists in the exception list then break out ...

	// if it is not found then add it to the Core Domains

	// Load the List of Exceptions First .. check whether it is empty or not.

}

func (domSecRecords *DomainSecurityRecordsList) GetHash() string {

	// Here we read in a list of exceptions from a file
	domSecRecords.LoadExceptions() // if not already loaded
	return getHashCRC64(domSecRecords.domainSecurityRecords)
}

func (domSecRecords *DomainSecurityRecordsList) GetHashDB() string {

	// Here we read in a list of exceptions from a file
	domSecRecords.LoadExceptions() // if not already loaded
	return getHashCRC64(domSecRecords.domainSecurityRecordsCore)
}

func getHashCRC32(dsr []dommainSecurityRecord) string {

	// Here we read in a list of exceptions from a file

	var hashString string = ""

	// Get the collation of the domain and security int as a string
	for _, domSecRecord := range dsr {
		hashString += domSecRecord.domainSuffix + strconv.Itoa(domSecRecord.securityLevelInt)
	}

	// Work Around for Govlink Publishing a record with a capital first letter which Firstwave does not support
	hashString = strings.toLower(hashString)

	crc32q := crc32.MakeTable(0xD5828281)

	uint32Result := crc32.Checksum([]byte(hashString), crc32q)

	resultString := strconv.FormatUint(uint64(uint32Result), 10)

	//fmt.Println(uint32Result)

	return resultString
}

func getHashCRC64(dsr []dommainSecurityRecord) string {

	// Here we read in a list of exceptions from a file

	var hashString string = ""

	// Get the collation of the domain and security int as a string
	for _, domSecRecord := range dsr {
		hashString += domSecRecord.domainSuffix + strconv.Itoa(domSecRecord.securityLevelInt)
	}

	// Work Around for Govlink Publishing a record with a capital first letter which Firstwave does not support
	hashString = strings.toLower(hashString)

	crc64q := crc64.MakeTable(crc64.ISO)

	uint64Result := crc64.Checksum([]byte(hashString), crc64q)

	resultString := strconv.FormatUint(uint64(uint64Result), 10)

	//fmt.Println(uint32Result)

	return resultString
}
