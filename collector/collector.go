package collector

// This package contains the main methods that are used to display data
// they use the govlinkCollector interfaces to display the data

// Get via the Domain Security Records List Via HTTP
func GetViaHTTP(serverAddress string) DomainSecurityRecordsList {

	httpCollector := govlinkHTTPCollector{HTTPLocation: serverAddress}
	domainSecurityRecordsList := GetDomainSecurityRecords(httpCollector)

	return domainSecurityRecordsList
}

// Get via JSON

// Get via CSV

// Get via DB

func GetViaDB(serverAddress, dbName, dbUsername, dbPassword string) DomainSecurityRecordsList {

	// look up here maybe???

	dbCollector := govlinkDBCollector{
		DBAddress:  serverAddress,
		DBName:     dbName,
		DBUsername: dbUsername,
		DBPassword: dbPassword}

	domainSecurityRecordsList := GetDomainSecurityRecords(dbCollector)

	return domainSecurityRecordsList
}
