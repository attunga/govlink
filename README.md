# Govlink Utilities

Govlink Utilities is a program that is used to display, export and hash data from govlink data sources, 


## Installation

Govlink is statically compiled so does not require any external dependencies to be available on the computer it is run 
under.

To install the program copy the govlink binary to /usr/loca/bin  

The program will then look for it's configuration file named govlink.yaml in the local directory or in /etc/govlink/

Copy the provided sample configuration file govlink.sample.yaml to govlink.yaml in your preferred location.   Then edit
the configuration options as shown below.

### Configuration Options

**collector:**  (options "http or db") This is the type of collector that we will get data from.  The http collector 
 gets its data from a html web page provided by Verison. The db collector receives its data from the Firstwave database.
 Although some configuration options are common such as collector, display or server address, other options such as 
 those related to databases will be unique to the db type of collector.

**format:**  (options "csv" or "full") This is the output display that will be shown to screen. It can be overridden by
 options passed at the command line.  Currently csv and full options are supported. The csv type is primarily for a
 succinct display that can be used for import or comparison.  The full option is best used when viewing or sorting 
 govlink entries. 

**sort:**  (options "domain", "org", "seclevel" or "sec-org")  This option allows the records to be displayed sorted on 
 specific fields.  The domain is the default option and is suitable in most cases. Other options will only work if the 
 fields are present.   At times such as when querying the database, not all fields will be present so the sort options 
 will appear to be not working. This option is best left disabled except in specific circumstances as it causes 
 additional sorts when records are read.  When using full then options can also be passed at the command line.

**serverAddress:** This is the server address to talk to.  When using a http source then it should be in http:// format,
 when connecting to a database it should be in the database format.  Examples of the different options can be seen in 
 the sample configuration file.

**dbName:** This is the database name to connection to when using a db connection.

**dbUsername:** This is the database username to use when using a db connection.
 
**dbPasswordIsEncrypted:** This is the unencrypted database password to use when connecting to a database unencrypted or
 when encrypting a database password for use with the dbEncryptedPassword option.  For more information on this see the 
 next section.
 
**dbPasswordIsEncrypted:** (options "True" or "False")  This option defines whether the system will use the encrypted 
 or unencrypted password options.
  
**dbEncryptedPassword:** This is the encrypted password to be used.  It is uniquely tied to this computer and cannot be
 used for database access on other computers.

**exceptionDomains:** This is a list of domains that are in the database but not in govlink.   Domains are often in the
 database to allow threshold control out of the platform for domains that are customer or local domains but are not
 Govlink domains.

###  Using Encrypted Database Passwords

 Encrypted passwords are AES256 encrypted passwords using a seed that derives from the local computers unique network
 and hardware characteristics. If IP addresses or hardware is changed on a server then this password may need to be 
 regenerated.
 
 To use encrypted passwords you must first set a plain text password and use the hash option such as 
 
 > govlink hash 

 You will then be presented with an encrypted password.  Copy this password into the field named **dbEncryptedPassword**.
 You should then set the option **dbPasswordIsEncrypted** to "True" and remove the password at **dbPasswordIsEncrypted**

 The system should then connect with the encrypted password and without the need for a password that is human readable
 in the configuration file.
 
## Usage

### compare

 This command is currently in development and is not used.

### Display

 The display command is used to display govlink records to screen.   The display command can also be used through the 
 command line to export records to disk for export or comparison with previous govlink records.  The display command can
 display records in csv or full formats.  The required configuration options can be retrieved from a configuration file
 making this command idea for automated operations.   Command line options can also be passed on the command line if 
 required.

#### Command Line Option

> Flags:   
   -f, --format string      Output Display Format - Currently csv, full or core for DB  (default "csv")  
   -h, --help               help for display  
   -o, --sortorder string   Sort Order - domain, org, seclevel or sec-org (default "domain")  

> Global Flags:  
   -c, --collector string       Collector type html or db (default "http")  
   --config string          config file (default is govlink.yml)  
   -d, --debug                  Additional Debugging Information  
   -s, --serverAddress string   Server to Connect to (default "http://localhost/UserList.html")  

#### Examples

Display records to screen retrieving options from configuation settings or flag defaults.
> govlink display

Display records to screen sorted by the security level Number.
> govlink display -o=seclevel

Display records to screen using full format sorted by the organisation owning the domain (from html).
> govlink display --format=full --sortorder=org


### Hash

This command generates an integer hash combined domains and security levels. Configuration is limited as it is expected
 that this command is run in an automated manner for monitoring when two data sources may be out of sync.  The hash is
 generated on the list of domains and security levels.   When using the DB source this list of domains will be those
 in the system minus the list of exception domains.

The hash generated is a 64 bit unsigned integer which derives from an ISO Standard CRC hash.

#### Command Line Option

> Global Flags:  
   -c, --collector string       Collector type html or db (default "http")  
   --config string          config file (default is govlink.yml)  
   -d, --debug                  Additional Debugging Information  
   -s, --serverAddress string   Server to Connect to (default "http://localhost/UserList.html")  

#### Examples

Displays a 64 bit integer hash to screen.
> govlink hash

### Encrypt

See section on using encrypted passwords