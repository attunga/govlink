package display

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"govlink/collector"
	"govlink/utils"
	"os"
)

func DisplayRecordsToScreen(collectorType, serverAddress, displayFormat, sortOrder string, cmd *cobra.Command) {

	var domSecRecords collector.DomainSecurityRecordsList

	// Get Data from Required Source
	switch collectorType {
	case "http":
		domSecRecords = collector.GetViaHTTP(serverAddress)
	case "db":
		// We Get the username and password direct from Settings for DB Connections
		// Because we don't really want these passed on the command line and then into command history.
		dbName := viper.GetString("dbName")
		dbUsername := viper.GetString("dbUsername")
		dbPassword := ""
		if viper.GetBool("dbPasswordIsEncrypted") {
			key := []byte(utils.GetUniqueIDStringKey())
			dbPassword = utils.Decrypt(key, viper.GetString("dbEncryptedPassword"))
		} else {
			dbPassword = viper.GetString("dbPassword")
		}

		//fmt.Println("Using DB Password:", dbPassword)

		// We could check if these exist .. bug out if they don't.
		domSecRecords = collector.GetViaDB(serverAddress, dbName, dbUsername, dbPassword)
	case "https":
		//domSecRecords := collector.GetViaHTTP()
	default:
		fmt.Println("Unknown Collector Type - valid options are http or db")
		os.Exit(1)
	}

	// Select the Sort Order
	switch sortOrder {
	case "domain":
		domSecRecords.SortByDomain()
	case "org":
		domSecRecords.SortByOrg()
	case "seclevel":
		domSecRecords.SortBySecuritylevel()
	case "sec-org":
		domSecRecords.SortBySecuritylevel()
		domSecRecords.SortByOrg()
	default:
		fmt.Println("Unknown Sort Order: use domain, org, seclevel or sec-org")
		os.Exit(1)
	}

	// Print in desired format
	switch displayFormat {
	case "csv":
		domSecRecords.PrintToScreenCSV()
	case "core":
		domSecRecords.PrintToScreenCSVCore()
	case "full":
		domSecRecords.PrintToScreenFull()
	default:
		fmt.Println("Unknown Format Type - valid options are http or db")
		os.Exit(1)
	}

}
